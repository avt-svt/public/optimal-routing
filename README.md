# Optimal Routing for Hybrid Vehicles

Thank you for using Optimal Routing for Hybrid Vehicles.
If you have any issues, concerns, or comments, please communicate them using the "Issues" functionality in [GitLab](https://git.rwth-aachen.de/avt-svt/public/optimal-routing) or send an e-mail to adrian.caspari@rwth-aachen.de.

## About

The project comprises the python files to setup and solve integrated optimal routing and powertrain operation problems for hybrid vehicles.

### Executable programs

The executable programs can be executed in python.
Using user input data, they solve the routing optimization problems and output the results.

Three executable programs are provided which can be executed using python:

* main_illustration.py
* main_longDistance.py
* main_urban.py

The executable programs use the source coude files placed in the folder "src".

* The file "solveRoutingOptProb.py" contains the source code for formulating and solving the routing optimization problem.
  The optimizatio problem is formulated in pyomo ans solved using Gurobi. 
  Other solvers can be selected upon convenience.
* The file "plotRoutingResults.py" contains the source code for plotting the routing optimization result.
	
The input files (edges.csv and nodes.csv) from the graph generator and the executable program results are stored in three folders corresponding to the three application programs:
	
* the folder "case_illustration" contains the input files and results of main_illustration.py
* the folder "case_longDistance" contains the input files and results of main_longDistance.py
* the folder "case_urban" contains the input files and results of main_urban.py


### Graph generator

The graph generator program is contained in folder "graphGenerator".
The program "graph_generator.py" can be executed in python.
The user has to enter input data, e.g., start and goal adddress.
The folder "graphGenerator" contains the source code for generating the input files (edges.csv and nodes.csv) for the application programs.

* The file "graph_generator.py" contains the source code for the graph generating program, which can be executed in python. 
  The graphGenerator uses the user input information (start and goal address) to download the directed graph information from the internet and generate the edges.csv and nodes.csv files.
  The program "graph_generator.py" outputs the edges and nodes of the directed graph in the files edges.csv and nodes.csv.
  These files can be used as input for the routing optimization program (executable programs).


## Usage

The programs in the project can be executed using python.
<p align="left"> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> </p>




## How to Cite This Work


```
@Article{Caspari2022OptimalRouting,
  author  = {Caspari, Adrian and Fahr, Steffen and Mitsos, Alexander},
  journal = {IEEE Transactions on Intelligent Transportation Systems},
  title   = {Optimal Eco-Routing for Hybrid Vehicles with Mechanistic/Data-Driven Powertrain Model Embedded},
  year    = {2022},
  volume  = {23},
  number  = {9},
  pages   = {14632-14648},
  doi     = {10.1109/TITS.2021.3131298},
}

```

## License
This project is licensed under the MIT License:
Copyright 2021 Adrian Caspari, Steffen Fahr, Alexander Mitsos (AVT Process Systems Engineering, RWTH Aachen University, 52074 Aachen, Germany, amitsos@alum.mit.edu).
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

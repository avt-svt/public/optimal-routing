Solver command line: ['C:\\gurobi910\\win64\\bin\\gurobi.bat']



--------------------------------------------

Warning: your license will expire in 2 days

--------------------------------------------



Using license file c:\gurobi910\gurobi.lic

Academic license - for non-commercial use only - expires 2021-02-28

Read LP format model from file C:\Users\AdrianC\AppData\Local\Temp\tmpvthcc7b0.pyomo.lp

Reading time = 0.33 seconds

x31297: 50637 rows, 31297 columns, 126802 nonzeros

Changed value of parameter NonConvex to 0

   Prev: -1  Min: -1  Max: 2  Default: -1

Changed value of parameter MIPFocus to 3

   Prev: 0  Min: 0  Max: 3  Default: 0

Changed value of parameter TIME_LIMIT to 10800.0

   Prev: inf  Min: 0.0  Max: inf  Default: inf

Changed value of parameter Cuts to 0

   Prev: -1  Min: -1  Max: 3  Default: -1

Gurobi Optimizer version 9.1.0 build v9.1.0rc0 (win64)

Thread count: 2 physical cores, 4 logical processors, using up to 4 threads

Optimize a model with 50637 rows, 31297 columns and 126802 nonzeros

Model fingerprint: 0xf3483a35

Variable types: 27693 continuous, 3604 integer (3604 binary)

Coefficient statistics:

  Matrix range     [9e-08, 7e+05]

  Objective range  [1e+00, 1e+00]

  Bounds range     [1e+00, 1e+06]

  RHS range        [3e-06, 7e+05]

Warning: Model contains large matrix coefficient range

         Consider reformulating model or setting NumericFocus parameter

         to avoid numerical issues.

Presolve removed 22166 rows and 14753 columns

Presolve time: 0.62s

Presolved: 28471 rows, 16544 columns, 85626 nonzeros

Variable types: 13105 continuous, 3439 integer (3439 binary)

Presolve removed 1755 rows and 1752 columns

Presolved: 26716 rows, 14792 columns, 80349 nonzeros





Root relaxation: objective 4.858344e+01, 14022 iterations, 0.49 seconds



    Nodes    |    Current Node    |     Objective Bounds      |     Work

 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time



     0     0   48.58344    0    2          -   48.58344      -     -    1s

H    0     0                     105.0651042   48.58344  53.8%     -    1s

     0     0   48.58344    0    2  105.06510   48.58344  53.8%     -    1s

H    0     0                     105.0641455   48.58344  53.8%     -    1s

H    0     0                     105.0641453   48.58344  53.8%     -    1s

     0     2   48.58660    0    2  105.06415   48.58660  53.8%     -    1s

    70    73   51.27920   16   41  105.06415   49.09997  53.3%   483    5s

   179   172   50.82796   11    7  105.06415   49.28092  53.1%   475   10s

   305   297   49.82729    9   39  105.06415   49.38707  53.0%   443   15s

   469   400 infeasible   22       105.06415   49.42433  53.0%   383   20s

H  577   468                     105.0641122   49.46928  52.9%   374   24s

   589   490 infeasible   16       105.06411   49.47797  52.9%   376   25s

H  591   490                     105.0641120   49.47797  52.9%   375   25s

H  623   511                     104.1326248   49.52220  52.4%   369   26s

H  631   511                     102.8701682   49.52220  51.9%   367   26s

   746   555   52.52208   26    2  102.87017   49.56305  51.8%   351   30s

   763   569   49.56305   21    3  102.87017   49.56305  51.8%   367   35s

   771   575   49.56305   25    7  102.87017   49.56305  51.8%   367   40s

   792   579   76.13293   32    4  102.87017   49.56305  51.8%   379   45s

   848   609   49.56305   18    2  102.87017   49.56305  51.8%   385   50s

   926   652   49.56305   21   20  102.87017   49.56305  51.8%   388   55s

   986   674 infeasible   22       102.87017   49.56305  51.8%   401   60s

  1033   688     cutoff   26       102.87017   49.56305  51.8%   408   65s

  1097   709     cutoff   26       102.87017   49.56305  51.8%   418   70s

  1140   702   53.31355   29   12  102.87017   49.56305  51.8%   431   75s

  1220   708     cutoff   24       102.87017   49.56305  51.8%   439   80s

  1269   705     cutoff   28       102.87017   49.63330  51.8%   448   86s

  1354   698 infeasible   22       102.87017   49.71206  51.7%   450   90s

  1489   673     cutoff   26       102.87017   49.77495  51.6%   449   96s

  1556   677   50.94870   21   29  102.87017   49.82502  51.6%   451  101s

  1613   669   49.83638   21   21  102.87017   49.83638  51.6%   462  106s

  1669   661   50.88504   24    7  102.87017   49.86055  51.5%   467  114s

  1716   656     cutoff   24       102.87017   49.88984  51.5%   470  118s

  1749   654   54.82162   25    6  102.87017   49.91484  51.5%   476  121s

  1784   654   50.59829   21    4  102.87017   49.93062  51.5%   481  129s

  1832   636 infeasible   23       102.87017   49.94729  51.4%   482  135s

  1877   624 infeasible   22       102.87017   49.99137  51.4%   487  140s

  1912   612   50.02050   25   24  102.87017   50.02050  51.4%   492  145s

  1948   614 infeasible   20       102.87017   50.04104  51.4%   495  151s

  1994   594 infeasible   27       102.87017   50.07675  51.3%   498  155s

  2092   553     cutoff   27       102.87017   50.14338  51.3%   505  163s

  2154   549 infeasible   25       102.87017   50.17435  51.2%   505  167s

  2215   525 infeasible   26       102.87017   50.23224  51.2%   508  171s

  2268   503     cutoff   29       102.87017   50.27242  51.1%   513  175s

  2335   478 infeasible   27       102.87017   50.29266  51.1%   514  180s

  2387   460 infeasible   23       102.87017   50.35076  51.1%   517  185s

  2442   437 infeasible   29       102.87017   50.38647  51.0%   519  190s

  2500   446   75.37179   28   59  102.87017   50.40971  51.0%   525  197s

  2542   437     cutoff   21       102.87017   50.43249  51.0%   526  203s

  2609   425   50.54330   23    7  102.87017   50.49300  50.9%   534  209s

  2676   434 infeasible   30       102.87017   50.52793  50.9%   537  216s

  2749   430   50.87301   27    4  102.87017   50.62479  50.8%   538  223s

  2825   420 infeasible   29       102.87017   50.69671  50.7%   543  230s

  2915   414 infeasible   22       102.87017   50.80992  50.6%   549  238s

  2973   401     cutoff   23       102.87017   50.84937  50.6%   554  245s

  3065   391   51.02494   23    6  102.87017   50.94244  50.5%   559  252s

  3180   383 infeasible   28       102.87017   51.04231  50.4%   560  259s

  3283   376   52.06791   22    3  102.87017   51.19642  50.2%   562  271s

  3400   367 infeasible   24       102.87017   51.25318  50.2%   564  280s

  3513   351 infeasible   26       102.87017   51.35772  50.1%   567  292s

  3625   361     cutoff   23       102.87017   51.43160  50.0%   573  308s

  3784   357   52.40591   23    3  102.87017   51.62812  49.8%   572  318s

  3920   355   52.60180   23    5  102.87017   51.83595  49.6%   574  339s

  4043   351 infeasible   32       102.87017   51.91009  49.5%   575  357s

  4218   342   54.69067   29    6  102.87017   52.42878  49.0%   574  369s

  4386   279   54.03554   23    4  102.87017   52.65557  48.8%   574  381s

  4554   237   53.75608   28    3  102.87017   53.37498  48.1%   574  398s

  4734   187   53.92839   29    7  102.87017   53.92135  47.6%   574  408s

  4930   166 infeasible   32       102.87017   54.84957  46.7%   574  419s

  5035   158 infeasible   30       102.87017   56.73274  44.9%   571  422s

  5115   106     cutoff   27       102.87017   73.70373  28.4%   571  425s

  5173    47     cutoff   26       102.87017   79.70837  22.5%   567  436s

  5433    51   84.55544   30   22  102.87017   84.36950  18.0%   552  446s



Explored 5767 nodes (3081244 simplex iterations) in 449.66 seconds

Thread count was 4 (of 4 available processors)



Solution count 5: 102.87 104.133 105.064 ... 105.065



Optimal solution found (tolerance 1.00e-04)

Best objective 1.028701682105e+02, best bound 1.028701682105e+02, gap 0.0000%


